# gentoo

Source-based Linux distribution https://gentoo.org/

# Gentoo documentation
* [*Gentoo AMD64 Handbook*
  ](https://wiki.gentoo.org/wiki/Handbook:AMD64)
* [*Portage*
  ](https://wiki.gentoo.org/wiki/Portage)
* [*emerge*
  ](https://dev.gentoo.org/~zmedico/portage/doc/man/emerge.1.html)
  man page

# Documentation from other sources
* https://www.google.com/search?q=how+to+run+gentoo+on+docker
* [*Gentoo as a Docker build system?*
  ](https://pasztor.at/blog/gentoo-docker)
